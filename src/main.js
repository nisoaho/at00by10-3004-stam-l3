const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

// endpoint localhost:3000/
app.get('/', (req, res) => {
  res.send('Hello World!')
})

// endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    // query return strings
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);

    // The brackets make it so that also the name of the variable is printed
    console.log({a,b})

    const total = mylib.sum(a,b)

    res.send(a.toString() + "  plus " + b.toString() + " is " + total.toString())
})

// endpoint localhost:3000/multiply?a=42&b=21
app.get('/multiply', (req, res) => {
  // query return strings
  const a = parseInt(req.query.a);
  const b = parseInt(req.query.b);

  // The brackets make it so that also the name of the variable is printed
  console.log({a,b})

  const multi = mylib.multiply(a,b)

  res.send(a.toString() + " times " + b.toString() + " is " + multi.toString())
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})