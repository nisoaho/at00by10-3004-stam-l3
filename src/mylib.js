module.exports = {
    sum: (a,b) => {
        return a+b
    },

    multiply: (a,b) => {
        return a*b
    },

    firstFunction: () => {
        console.log('This is a function running before the testing starts.')
    },

    lastFunction: () => {
        console.log('This is a function running after the testing.')
    }

}