const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('unit testing mylib.js', () => {

    let myvar = undefined;
    // This is used if there are several steps to do before running the test.
    before(() => {
        myvar = 1;
        mylib.firstFunction()
    })

    // Testing sum function with expect
    it('Should return 2 when using sum function with a=1 and b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })

    // Testing multiply function with assert
    it('Should return 4 when using multiply function with a=2 and b=2', () => {
        const result = mylib.multiply(2,2)
        assert.equal(result, 4)
    })
    
    // Better than commenting out tests is to skip them -> helps not to forget them
    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })

    // Here the variable was created in the before part of the test. 
    // Exist could be probably used to test also if a function returns a variable or
    // for example if a variable was created properly.
    it('myvar should exist', () => {
        should.exist(myvar)
    })

    // Here is only a console.log but this could probably be also some runnin some functions.
    // The purpose is to clean up after the tests.
    after(() => {
        //console.log('Hello')
        mylib.lastFunction()
    })
})